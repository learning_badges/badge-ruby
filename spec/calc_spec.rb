require 'calc'
require 'spec_helper'

RSpec.describe Calc, "#add" do
    context "sum two integer" do
        it "sums 1+1" do
            calc = Calc.new
            result = calc.add(1,1)
            expect(result).to eq(2)
        end
    end
end 