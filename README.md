# Badge Ruby

[![pipeline status](https://gitlab.com/learning_badges/badge-ruby/badges/main/pipeline.svg)](https://gitlab.com/learning_badges/badge-ruby/-/commits/main) [![coverage report](https://gitlab.com/learning_badges/badge-ruby/badges/main/coverage.svg)](https://gitlab.com/learning_badges/badge-ruby/-/commits/main)

## How to

```
bundle install
rspec
```